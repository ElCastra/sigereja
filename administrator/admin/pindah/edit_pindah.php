<?php

if (isset($_GET['kode'])) {
	$sql_cek = "SELECT * FROM tb_pindah a join tb_umat b on a.id_umat=b.id_umat WHERE a.id_pindah='" . $_GET['kode'] . "'";
	$query_cek = mysqli_query($koneksi, $sql_cek);
	$data_cek = mysqli_fetch_array($query_cek, MYSQLI_BOTH);
}
?>

<div class="card card-warning">
	<div class="card-header">
		<h3 class="card-title">
			<i class="fa fa-edit"></i> Ubah Data Perpindahan
		</h3>
	</div>
	<form action="" method="post" enctype="multipart/form-data">
		<div class="card-body">

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">No Sistem</label>
				<div class="col-sm-2">
					<input type="text" class="form-control" id="id_pindah" name="id_pindah" value="<?php echo $data_cek['id_pindah']; ?>" readonly />
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">NIK</label>
				<div class="col-sm-6">
					<!-- <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK" required> -->
					<select name="f_nik" id="f_nik" class="form-control select2bs4" required>
						<option value="" disabled selected>- Pilih NIK -</option>
						<?php
						// ambil data dari database
						$query = "select * from tb_umat where nik";
						$hasil = mysqli_query($koneksi, $query);
						while ($row = mysqli_fetch_array($hasil)) {
						?>
							<option value="<?php echo $row['id_umat'] ?>" <?php echo $row['id_umat'] == $data_cek['id_umat'] ? "selected" : "" ?>>
								<?php echo $row['nik'] ?>
							</option>
						<?php
						}
						?>
					</select>
				</div>
				<div class="col-sm-2">
					<button id="btn_nik" class="btn btn-primary" type="button"> Cari</button>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" value="<?php echo $data_cek['nama_umat'] ?>" id="nama_umat" name="nama_umat" placeholder="Nama Umat" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">KUB Asal</label>
				<div class="col-sm-4">
					<select name="id_kub" id="id_kub" class="form-control select2bs4" required>
						<option value="" disabled selected>- Pilih KUB -</option>
						<?php
						// ambil data dari database
						$query = "select * from tb_kub where id_kub";
						$hasil = mysqli_query($koneksi, $query);
						while ($row = mysqli_fetch_array($hasil)) {
						?>
							<option value="<?php echo $row['id_kub'] ?>"  <?php echo $row['id_kub'] == $data_cek['id_kub_asal'] ? "selected" : "" ?>>
								<?php echo $row['nama_kub'] ?>
							</option>
						<?php
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Lingkungan Asal</label>
				<div class="col-sm-4">
					<select name="id_lingkungan" id="id_lingkungan" class="form-control select2bs4" required>
						<option value="" disabled selected>- Pilih Lingkungan -</option>
						<?php
						// ambil data dari database
						$query = "select * from tb_lingkungan where id_lingkungan";
						$hasil = mysqli_query($koneksi, $query);
						while ($row = mysqli_fetch_array($hasil)) {
						?>
							<option value="<?php echo $row['id_lingkungan'] ?>"  <?php echo $row['id_lingkungan'] == $data_cek['id_lingkungan_asal'] ? "selected" : "" ?>>
								<?php echo $row['nama_lingkungan'] ?>
							</option>
						<?php
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">KUB Tujuan</label>
				<div class="col-sm-4">
					<select name="id_kub_tujuan" id="id_kub_tujuan" class="form-control select2bs4" required>
						<option value="" disabled selected>- Pilih KUB -</option>
						<?php
						// ambil data dari database
						$query = "select * from tb_kub where id_kub";
						$hasil = mysqli_query($koneksi, $query);
						while ($row = mysqli_fetch_array($hasil)) {
						?>
							<option value="<?php echo $row['id_kub'] ?>"  <?php echo $row['id_kub'] == $data_cek['id_kub_tujuan'] ? "selected" : "" ?>>
								<?php echo $row['nama_kub'] ?>
							</option>
						<?php
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Lingkungan Tujuan</label>
				<div class="col-sm-4">
					<select name="id_lingkungan_tujuan" id="id_lingkungan_tujuan" class="form-control select2bs4" required>
						<option value="" disabled selected>- Pilih Lingkungan -</option>
						<?php
						// ambil data dari database
						$query = "select * from tb_lingkungan where id_lingkungan";
						$hasil = mysqli_query($koneksi, $query);
						while ($row = mysqli_fetch_array($hasil)) {
						?>
							<option value="<?php echo $row['id_lingkungan'] ?>" <?php echo $row['id_lingkungan'] == $data_cek['id_lingkungan_tujuan'] ? "selected" : "" ?>>
								<?php echo $row['nama_lingkungan'] ?>
							</option>
						<?php
						}
						?>
					</select>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<input type="submit" name="Ubah" value="Simpan" class="btn btn-success">
			<a href="?page=data-kub" title="Kembali" class="btn btn-secondary">Batal</a>
		</div>
	</form>
</div>

<?php

if (isset($_POST['Ubah'])) {
	// $sql_ubah = "UPDATE tb_kematian SET 
	// 	nik='" . $_POST['f_nik'] . "',
	// 	nama='" . $_POST['nama_umat'] . "',
	// 	id_lingkungan='" . $_POST['id_lingkungan'] . "',
	// 	id_kub='" . $_POST['id_kub'] . "',
	// 	tempat_kematian='" . $_POST['tempat_kematian'] . "',
	// 	tanggal_kematian='" . $_POST['tanggal_kematian'] . "',
	// 	status_sakramen='" . $_POST['status_sakramen'] . "'
	// 	WHERE id_kematian='" . $_POST['id_kematian'] . "'";
	// $query_ubah = mysqli_query($koneksi, $sql_ubah);
	// mysqli_close($koneksi);
	$id_pindah = $_POST['id_pindah'];
	$id_umat = $_POST['f_nik'];
	$nama_umat = $_POST['nama_umat'];
	$id_kub_asal = $_POST['id_kub'];
	$id_lingkungan_asal = $_POST['id_lingkungan'];
	$id_kub_tujuan = $_POST['id_kub_tujuan'];
	$id_lingkungan_tujuan = $_POST['id_lingkungan_tujuan'];
	$sql_simpan = "UPDATE tb_pindah set id_umat='$id_umat',id_kub_asal='$id_kub_asal', id_lingkungan_asal='$id_lingkungan_asal', id_kub_tujuan='$id_kub_tujuan',id_lingkungan_tujuan='$id_lingkungan_tujuan' where id_pindah='$id_pindah'";
	$query_ubah = mysqli_query($koneksi, $sql_simpan);

	$sql_pindah = "UPDATE tb_umat set id_kub='$id_kub_tujuan', id_lingkungan='$id_lingkungan_tujuan' where id_umat='$id_umat'";
	$query_pindah = mysqli_query($koneksi, $sql_pindah);

	if ($query_ubah && $query_pindah) {
		echo "<script>
      Swal.fire({title: 'Ubah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
      }).then((result) => {if (result.value)
        {window.location = 'index.php?page=data-pindah';
        }
      })</script>";
	} else {
		echo "<script>
      Swal.fire({title: 'Ubah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
      }).then((result) => {if (result.value)
        {window.location = 'index.php?page=data-pindah';
        }
      })</script>";
	}
}
